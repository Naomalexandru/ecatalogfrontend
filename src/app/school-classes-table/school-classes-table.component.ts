import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { SchoolClassDto } from 'src/dtos/schoolClassDto';


const SCHOOL_CLASSES: SchoolClassDto[] = [
  {id: "1", startYear: 2022, name: "A", classLevel: "MIDDLE_SCHOOL", normalizedSchoolClassName: "a 6-a A" },
  {id: "2", startYear: 2022, name: "B", classLevel: "MIDDLE_SCHOOL", normalizedSchoolClassName: "a 6-a B" },
  {id: "3", startYear: 2022, name: "C", classLevel: "MIDDLE_SCHOOL", normalizedSchoolClassName: "a 6-a C" }
];

@Component({
  selector: 'app-school-classes-table',
  templateUrl: './school-classes-table.component.html',
  styleUrls: ['./school-classes-table.component.css']
})
export class SchoolClassesTableComponent {
  partialColumns =['id','normalizedSchoolClassName','startYear','classLevel']
  dataSource :SchoolClassDto[]=[];

constructor(private httpClient:HttpClient){

}

  ngOnInit():void{
  this.httpClient.get("/api/school-class").subscribe(response=>{
    console.log(response);
    this.dataSource = response as SchoolClassDto[];
  })
  }
}

