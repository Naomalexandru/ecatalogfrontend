import { Component, Input } from '@angular/core';
import { StudentCardDto } from 'src/dtos/studentCardDto';

@Component({
  selector: 'app-student-card',
  templateUrl: './student-card.component.html',
  styleUrls: ['./student-card.component.css']
})
export class StudentCardComponent {

  @Input()
  student: StudentCardDto= {
    id: "",
    firstName: "",
    lastName: "",
    schoolClassDto:{},
    profilePicture:"",
    schoolClassId:""
  };
}
