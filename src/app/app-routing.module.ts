import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolClassesTableComponent } from './school-classes-table/school-classes-table.component';
import { TeacherComponent } from './teacher/teacher.component';
import { TeacherCardListComponent } from './teacher-card-list/teacher-card-list.component';
import { TeacherFormComponent } from './teacher-form/teacher-form.component';
import { StudentCardListComponent } from './student-card-list/student-card-list.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { UserLoginPageComponent } from './user-login-page/user-login-page.component';

const routes: Routes = [
  {path:'classes',component:SchoolClassesTableComponent},
  {path:'teachers',component:TeacherCardListComponent},
  {path:'new-teacher',component:TeacherFormComponent},
  {path:'students',component:StudentCardListComponent},
  {path:'new-student',component:StudentFormComponent},
  {path:'login',component:UserLoginPageComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
