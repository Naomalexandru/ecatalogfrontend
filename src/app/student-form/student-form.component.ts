import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent {

  studentForm: FormGroup = new FormGroup({
    firstName :new FormControl(),
    lastName: new FormControl(),
    cnp: new FormControl(),
    profilePicture: new FormControl(),
    schoolClass : new FormControl(),
    schoolClassId: new FormControl()
  });
  constructor(private httpClient: HttpClient){

  }
  saveStudent(){
    var student={
      firstName: this.studentForm.value.firstName,
      lastName: this.studentForm.value.lastName,
      cnp: this.studentForm.value.cnp,
      schoolClass: this.studentForm.value.schoolClass,
      profilePicture: this.studentForm.value.profilePicture,
      schoolClassId: this.studentForm.value.schoolClassId
    }
    this.httpClient.post("/api/students",student)
    .subscribe(response=>{console.log(response);
    alert("Student was saved")
  });
  }
}
