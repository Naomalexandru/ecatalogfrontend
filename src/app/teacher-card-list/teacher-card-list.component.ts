import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { TeacherCardDto } from 'src/dtos/teacherCardDto';

@Component({
  selector: 'app-teacher-card-list',
  templateUrl: './teacher-card-list.component.html',
  styleUrls: ['./teacher-card-list.component.css']
})
export class TeacherCardListComponent {

  teacherCardList: TeacherCardDto[]=[];

  constructor (private httpClient:HttpClient){}

ngOnInit(): void{
  this.httpClient.get("/api/teachers/teachers_cards").subscribe(
    response=>{
      console.log(response);
      this.teacherCardList=response as TeacherCardDto[];
    }
  );
}
}
