import { SchoolClassDto } from "./schoolClassDto";

export class StudentCardDto {
    id?: string;
    firstName?: string;
    lastName?: string;
    schoolClassDto?: SchoolClassDto;
    profilePicture?: string;
    schoolClassId?: string;
}